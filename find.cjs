function find(input,fun){
    if(input.length==0){
        return 0;
    }


    let value=undefined;
    for(let vali=0;vali<input.length;vali++){
        if(fun(input[vali],vali,input)){
            value=input[vali];
        }
    }

    return value;
}

module.exports=find;