function filter(input,fun){
    if(input.length==0){
        return [];
    }


    let arr=[];
    for(let vali=0;vali<input.length;vali++){
        if(fun(input[vali],vali,input) === true){
            arr.push(input[vali]);
            
        }
    }
    return arr;
}
module.exports=filter;