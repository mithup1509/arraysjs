function each(input,fun){


    if(input.length==0){
        return [];
    }
    let arr=[];
    for(let vali=0;vali<input.length;vali++){
        arr.push(fun(input[vali],vali,input));
    }
    return arr;
}

module.exports=each;